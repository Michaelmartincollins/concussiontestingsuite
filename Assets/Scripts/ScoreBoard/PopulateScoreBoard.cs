﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulateScoreBoard : MonoBehaviour
{
    public GameObject listPrefab;
    public Transform container;

    public void Start()
    {
        foreach(KeyValuePair<string, float> key in ScoreManager.Instance.scores)
        {
            GameObject listClone = Instantiate(listPrefab, container);
            listClone.GetComponentsInChildren<Text>()[0].text = key.Key;
            listClone.GetComponentsInChildren<Text>()[1].text = key.Value.ToString();
        }
    }
}
