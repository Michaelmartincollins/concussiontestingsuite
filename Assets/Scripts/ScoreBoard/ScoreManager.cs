﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
    public Dictionary<string, float> scores = new Dictionary<string, float>();

    public void AddScoreToDictionary(string game, float score)
    {
        scores.Add(game, score);
    }

    public void ClearScores()
    {
        scores.Clear();
    }
}
