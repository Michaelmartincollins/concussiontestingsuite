﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameInstructions : MonoBehaviour
{
    public Text instructionText;
    private AudioSource audioSource;
    public float currentwaitTime = 0;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public IEnumerator TriggerInstructions(AudioClip audioClips, string instructions)
    {

            instructionText.text = instructions;

            audioSource.clip = audioClips;
            currentwaitTime = audioSource.clip.length;
            audioSource.Play();
            yield return new WaitForSeconds(audioSource.clip.length);

        instructionText.text = "";
        audioSource.clip = null;
    }
}
