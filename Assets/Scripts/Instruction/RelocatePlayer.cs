﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelocatePlayer : MonoBehaviour
{
    private GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");
        Transform spawnArea = GameObject.Find("SpawnNode").transform;
        player.transform.position = spawnArea.position;
    }
}
