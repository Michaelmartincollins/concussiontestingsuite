﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [Header("Shell Game")]
    internal int shellGameIndex = 0;
    public int[] cupCounts;
    public float[] gameSpeeds;
    public float[] guessLengths;

    [Header("HeadySteady")]
    public float gameSpeedModifier;

    internal enum GameState { Menu, ShellGame, HeadySteady, ScoreBoard };
    internal GameState gameState = GameState.Menu;

    internal bool cupGameComplete, headySteadyComplete;

    public void AddShellGameIndex()
    {
        if (shellGameIndex + 1 < cupCounts.Length)
        {
            print((shellGameIndex + 1) + " " + cupCounts.Length);
            shellGameIndex++;
            SceneManager.LoadScene("ShellGame");
        }
        else
        {
            cupGameComplete = true;
            SetNextScene();
        }
    }

    public void SetNextScene()
    {
        if (cupGameComplete && headySteadyComplete)
        {
            SceneManager.LoadScene("ScoreScreen");
        } else if (cupGameComplete && !headySteadyComplete)
        {
            SceneManager.LoadScene("HeadySteady");
        } else if(!cupGameComplete && headySteadyComplete)
        {
            SceneManager.LoadScene("ShellGame");
        }
    }

    private void OnLevelWasLoaded()
    {
        string level = SceneManager.GetActiveScene().name;

        switch (level)
        {
            case "MainMenu":
                gameState = GameState.Menu;
                break;
            case "HeadySteady":
                gameState = GameState.HeadySteady;
                break;
            case "ShellGame":
                gameState = GameState.ShellGame;
                break;
            case "ScoreBoard":
                gameState = GameState.ScoreBoard;
                break;
            default:
                break;
        }
    }
}