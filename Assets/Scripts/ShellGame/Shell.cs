﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour, I_Interactable
{
    public bool takingInputs = false;
    private ShellGame myGame;
    private GameObject myBall;

    private void Awake()
    {
        myGame = FindObjectOfType<ShellGame>();
    }

    public void OnSelect()
    {
        GetComponentInParent<ShellGroup>().CheckSelectedShell(this);
    }

    public void ToggleBall()
    {
        if (myBall == null)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                GameObject ball = GetComponentInParent<ShellGroup>().ballPrefab;
                GameObject ballClone = Instantiate(ball,
                    new Vector3(transform.position.x, hit.normal.y +
                        (ball.GetComponentInChildren<Renderer>().bounds.size.y / 2), transform.position.z),
                        Quaternion.identity) as GameObject;
                myBall = ballClone;
            }
        } else
        {
            Destroy(myBall);
            myBall = null;
        }
    }

    public bool TakingInputs()
    {
        if(takingInputs)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
