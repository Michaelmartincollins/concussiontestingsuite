﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellGroup : MonoBehaviour
{
    public Shell[] shells;
    public Shell goalShell = null;
    public GameObject ballPrefab;
    public bool waitingForInput = false;

    private Animation anim;
    private float animationSpeedModifier = 0;


    private void Awake()
    {
        anim = GetComponent<Animation>();
        shells = GetComponentsInChildren<Shell>();
        ChooseGoalShell();
    }

    public float GetAnimationTime()
    {
        if(animationSpeedModifier == 0)
        {
            return 0;
        } else
        {
            return anim.clip.length / animationSpeedModifier;
        }
    }

    public void PlayAnimation()
    {
        anim.Play();
    }

    public void SetAnimSpeed(float speed)
    {
        animationSpeedModifier = speed;
        anim["ShellAnimation" + shells.Length].speed = speed;
    }

    public void ChooseGoalShell()
    {
        goalShell = shells[Random.Range(0, shells.Length)];
        goalShell.ToggleBall();
    }

    public void ToggleBall()
    {
        goalShell.ToggleBall();
    }

    public void TogglePlayAnimation()
    {
        if(anim.isPlaying)
        {
            anim.Stop();
            waitingForInput = true;
        } else
        {
            anim.Play();
            waitingForInput = false;
        }
    }

    public void CheckSelectedShell(Shell shell)
    {
        ToggleShellsTakingInputs();
        FindObjectOfType<ShellGame>().playerHasChosenShell = true;
        if (shell == goalShell)
        {
            print("win");
        } else
        {
            print("loss");
        }
    }

    public void ToggleShellsTakingInputs()
    {
        foreach(Shell s in shells)
        {
            s.takingInputs = !s.takingInputs;
        }
    }
}
