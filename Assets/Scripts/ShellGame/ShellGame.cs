﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShellGame : MonoBehaviour
{
    [Header("Round Timers")]
    public float delayBeforeGameStart = 5.0f;

    [Header("Game Instructions")]
    public AudioClip instructionClip;
    public string instructionText;
    private GameInstructions gameInstructions;

    private bool canInteractWithShells;

    [SerializeField] internal bool playerHasChosenShell = false;

    public ShellGroup[] shellGroups;
    [SerializeField] private ShellGroup currentShellGroup;

    private void Start()
    {
        gameInstructions = FindObjectOfType<GameInstructions>();
        currentShellGroup = shellGroups[GameManager.Instance.cupCounts[GameManager.Instance.shellGameIndex] - 2];
        foreach (ShellGroup sg in shellGroups) { sg.gameObject.SetActive(false); }
        Invoke("TriggerStartGame", delayBeforeGameStart);
    }

    private void TriggerStartGame()
    {
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        if (GameManager.Instance.shellGameIndex == 0)
        {
            StartCoroutine(gameInstructions.TriggerInstructions(instructionClip, instructionText));
            yield return new WaitForSeconds(gameInstructions.currentwaitTime);
        }

        //Turn on the game object to be used.
        currentShellGroup.gameObject.SetActive(true);
        //Set the speed of the animation to the game speed.
        currentShellGroup.SetAnimSpeed(GameManager.Instance.gameSpeeds[GameManager.Instance.shellGameIndex]);
        //Start the animation.
        currentShellGroup.PlayAnimation();
        //Wait until the game is looking for inpout.
        while (!currentShellGroup.waitingForInput)
            yield return null;
        //Allow the player to interact with the cups for X seconds.
        float logTime = Time.time;
        //Allow for the player to make a selection.
        currentShellGroup.ToggleShellsTakingInputs();
        while (Time.time - logTime < GameManager.Instance.guessLengths[GameManager.Instance.shellGameIndex] && !playerHasChosenShell)
        {
            yield return null;
        }
        //Stop the player from being able to interact.
        currentShellGroup.ToggleShellsTakingInputs();
        currentShellGroup.TogglePlayAnimation();
        GameManager.Instance.AddShellGameIndex();
    }
}