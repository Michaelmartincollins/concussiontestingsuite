﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HeadySteady : MonoBehaviour
{
    public float baselineCaptureTime = 10.0f;

    public Animation animation;

    public AudioClip introInstructionClip;
    public string introInstructionText;

    public AudioClip testInstructionClip;
    public string testingInstructionText;

    private float averageBaselineMovement;
    private float averageTestedMovement;

    private GameObject headObject;

    private GameInstructions gameInstructions;

    public AudioClip rockSong;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameInstructions = FindObjectOfType<GameInstructions>();
        headObject = Camera.main.gameObject;
        StartCoroutine(GameLoop());
    }

    private IEnumerator GameLoop()
    {
        //Show the instruction text and play the instructional voice clips.
        StartCoroutine(gameInstructions.TriggerInstructions(introInstructionClip, introInstructionText));
        //Wait for the instructions to finish.
        yield return new WaitForSeconds(gameInstructions.currentwaitTime);

        audioSource.clip = rockSong;
        audioSource.Play();

        //Get current time for timer.
        float loggedTime = Time.time;

        //Get the timer object to use to display amount of timer remaining.
        Text timer = GameObject.Find("Timer").GetComponent<Text>();

        //Create a list to store the variation values.
        List<float> movementVariation = new List<float>();
        Vector3 startLocation = headObject.transform.position;
        while (Time.time - loggedTime < baselineCaptureTime)
        {
            //Show timer in text.
            movementVariation.Add(Vector3.Distance(startLocation, headObject.transform.position));
            timer.text = Mathf.Round(Time.time - loggedTime).ToString();
            yield return null;
        }
        timer.text = "";
        float average = 0;
        float[] distances = movementVariation.ToArray();
        for (int i = 0; i < distances.Length; i++)
        {
            average += distances[i];
        }
        averageBaselineMovement = average / movementVariation.Count;
        print(averageBaselineMovement);
        //Show instructional clip and play audio.

        audioSource.Pause();

        StartCoroutine(gameInstructions.TriggerInstructions(testInstructionClip, testingInstructionText));
        yield return new WaitForSeconds(gameInstructions.currentwaitTime);

        audioSource.UnPause();

        movementVariation.Clear();
        //potnetially change to be an average.
        startLocation = headObject.transform.position;
        loggedTime = Time.time;

        animation.Play();

        while(Time.time - loggedTime < animation.clip.length)
        {
            movementVariation.Add(Vector3.Distance(startLocation, headObject.transform.position));
            timer.text = Mathf.Round(Time.time - loggedTime).ToString();
            yield return null;
        }
        timer.text = "";
        animation.Stop();
        average = 0;
        distances = movementVariation.ToArray();
        for (int i = 0; i < distances.Length; i++)
        {
            average += distances[i];
        }
        averageTestedMovement = average / movementVariation.Count;
        print(averageTestedMovement);
        //Report the results as normalized value, "Your head moved X * more in the simulation."

        audioSource.Stop();

        //remove 
        float difference = averageTestedMovement - averageBaselineMovement;

        yield return new WaitForSeconds(5.0f);
        GameManager.Instance.headySteadyComplete = true;
        float percentageDifference = ((averageTestedMovement - averageBaselineMovement) / averageTestedMovement) / 100;
        ScoreManager.Instance.AddScoreToDictionary("HeadySteady", percentageDifference);
        GameManager.Instance.SetNextScene();
    }
}

