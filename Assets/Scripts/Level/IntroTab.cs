﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroTab : MonoBehaviour, I_Interactable
{
    public string levelName;
    public bool takingInputs = true;
    public void OnSelect()
    {
        GetComponent<AudioSource>().Play();
        SceneManager.LoadScene("Scenes/Games/" + levelName);
    }

    public bool TakingInputs()
    {
        if(takingInputs)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
