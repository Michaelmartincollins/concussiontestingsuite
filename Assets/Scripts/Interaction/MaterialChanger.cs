﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChanger : MonoBehaviour
{
    public bool beingSelected = true;
    public Material selectedMaterial;

    [SerializeField] private Renderer[] myRenderers;
    [SerializeField] private Material[] originalMaterials;
    [SerializeField] private Material[] copiedOriginalMaterials;

    private bool cr_isRunning;

    private void Start()
    {
        myRenderers = GetComponentsInChildren<Renderer>();
        //save all materials into original materials
        originalMaterials = new Material[myRenderers.Length];
        for (int i = 0; i < myRenderers.Length; i++)
        {
            originalMaterials[i] = Instantiate(myRenderers[i].material);
        }
    }

    public void TriggerMaterialChange()
    {
        StartCoroutine(MaterialChangeProcess());
    }

    private void Update()
    {
        if (beingSelected == true && cr_isRunning == false)
        {
            TriggerMaterialChange();
        }

        if (beingSelected == false && cr_isRunning)
        {
            StopAllCoroutines();
            cr_isRunning = false;
            ResetMaterial();
        }
    }

    private IEnumerator MaterialChangeProcess()
    {
        cr_isRunning = true;

        //duplicate original materials
        copiedOriginalMaterials = new Material[originalMaterials.Length];
        for (int i = 0; i < originalMaterials.Length; i++)
            copiedOriginalMaterials[i] = Instantiate(originalMaterials[i]);

        //set the materials to duplicated materials
        for (int i = 0; i < myRenderers.Length; ++i)
            myRenderers[i].material = copiedOriginalMaterials[i];

        float duration = 0;

        while (beingSelected && duration < 1.0f)
        {
            //lerp the duplicated materials to the selectedMaterial
            for (int i = 0; i < myRenderers.Length; ++i)
            {
                myRenderers[i].material.Lerp(myRenderers[i].material, selectedMaterial, duration);
            }

            duration += Time.deltaTime;

            //wheck if the user is till looking at the object

            yield return null;
        }

        yield return null;
    }

    public void ResetMaterial()
    {
        for (int i = 0; i < myRenderers.Length; ++i)
        {
            myRenderers[i].material = originalMaterials[i];
        }
    }
}
