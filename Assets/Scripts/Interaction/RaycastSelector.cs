﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastSelector : MonoBehaviour
{
    public float pointerSelectorTimer = 2.5f;
    public Image selectionIcon;

    private bool selecting;
    private Collider currentHit;

    private void Start()
    {
        selectionIcon.enabled = false;

    }
    private void Update()
    {
        CastRay();
    }

    private void CastRay()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider != null)
                currentHit = hit.collider;
        } else
        {
            currentHit = null;
        }

        if (Physics.Raycast(ray, out hit) && hit.collider.GetComponent<I_Interactable>() != null && selecting == false 
            && hit.collider.GetComponent<I_Interactable>().TakingInputs())
        {
            StartCoroutine(Selecting(hit.collider.gameObject));
        }
    }

    IEnumerator Selecting(GameObject interactedItem)
    {
        selecting = true;
        float logTime = Time.time;
        selectionIcon.enabled = true;
        while(selecting && Time.time - logTime < pointerSelectorTimer)
        {
            float fillAmount = (Time.time - logTime) / pointerSelectorTimer;
            selectionIcon.fillAmount = fillAmount;
            if (currentHit != interactedItem.GetComponent<Collider>()) { selecting = false; }
            yield return null;
        }

        if(selecting) interactedItem.GetComponent<I_Interactable>().OnSelect();
        selectionIcon.enabled = false;
        selecting = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.forward * 1000);
    }
}
